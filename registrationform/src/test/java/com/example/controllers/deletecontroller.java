@DeleteMapping(value = "/users/{id}/delete")
    public String deleteUser(@PathVariable("id") Long idx, final RedirectAttributes redirectAttributes) {

        logger.debug("Delete customer with Id {}", idx);

        redirectAttributes.addFlashAttribute("css", "Success");
        redirectAttributes.addFlashAttribute("msg", "The user is deleted");

      
        userService.delete(idx);
        return "redirect:/users/";
    }
 