package com.sunbeam.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;

import com.sunbeam.entities.customer;

public interface customerDao extends JpaRepository<custid, Integer> {
	
	@Query("select distinct c.custid from customer id")
	List<String> findDistinctcustomer();
	
	List<Customer> findBycustomerid(String customer);
	
	@Procedure(procedureName = "findCustId")
	Double callcustById(int id);
}

 
